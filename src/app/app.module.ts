import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './Nav-Bar/Nav-Bar.component';
import { CaroselComponent } from './carosel/carosel.component';
import { HeadingParaComponent } from './heading-para/heading-para.component';

@NgModule({
  declarations: [			
    AppComponent,
      NavBarComponent,
      CaroselComponent,
      HeadingParaComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
